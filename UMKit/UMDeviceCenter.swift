import ORSSerial

extension Notification.Name {
    public static let UMDeviceDidConnected = Notification.Name(rawValue: "com.scchnxx.UMKit.UMDeviceDidConnected")
    public static let UMDeviceDidDisconnected = Notification.Name(rawValue: "com.scchnxx.UMKit.UMDeviceDidDisconnected")
    public static let UMDeviceCenterDidStopScanning = Notification.Name(rawValue: "com.scchnxx.UMKit.UMDeviceCenterDidStopScanning")
}

public class UMDeviceCenter {
    
    private typealias DeviceInfoHandler = ((serialNumber: String, version: String, deviceModel: UMDevice.DeviceModel)?) -> Void
    
    public static let shared = UMDeviceCenter()
    public static let connectedDeviceKey = "connectedDeviceKey"
    public static let disconnectedDeviceKey = "disconnectedDeviceKey"
    
    public private(set) var isCancelling = false
    public private(set) var isScanning = false
    public private(set) var devices = [UMDevice]()
    
    private func post(name: Notification.Name, userInfo: [AnyHashable: Any]? = nil) {
        NotificationCenter.default.post(name: name, object: self, userInfo: userInfo)
    }
    
    private func device(with serialPort: ORSSerialPort) -> UMDevice? {
        devices.first { device in
            device.connection.serialPort == serialPort
        }
    }
    
    private func fetchDeviceInfo(connection con: UMConnection, _ completion: @escaping DeviceInfoHandler) {
        let group = DispatchGroup()
        var serialNumber: String?
        var version: String?
        
        group.enter()
        con.fetch(with: UMDevice.Attribute.serialNumber.packets()) { result in
            defer { group.leave() }
            guard case .success(let data) = result else { return }
            serialNumber = UMDevice.Attribute.serialNumber.serialNumber(data: data)
        }
        
        group.enter()
        con.fetch(with: UMDevice.Attribute.version.packets()) { result in
            defer { group.leave() }
            guard case .success(let data) = result else { return }
            version = UMDevice.Attribute.version.version(data: data)
        }
        
        group.notify(queue: .main) {
            guard let serialNumber = serialNumber, let version = version,
                let deviceModel = UMDevice.DeviceModel(serialNumber: serialNumber) else { return completion(nil) }
            let info = (serialNumber, version, deviceModel)
            completion(info)
        }
    }
    
    private func genDevice(with serialPort: ORSSerialPort, _ completion: @escaping (UMDevice?) -> Void) {
        let device = self.device(with: serialPort)
        let con = (device?.connection ?? UMConnection(serialPort: serialPort))
        
        con.open { success in
            guard success else {
                con.close()
                completion(nil)
                return
            }
            
            self.fetchDeviceInfo(connection: con) { info in
                guard let info = info else {
                    con.close()
                    completion(nil)
                    return
                }
                
                if let device = device {
                    completion(device)
                } else {
                    let device = UMDevice(connection: con,
                                          serialNumber: info.serialNumber,
                                          version: info.version,
                                          deviceModel: info.deviceModel)
                    completion(device)
                }
            }
        }
    }
    
    private func refreshDevices() {
        let serialPorts = ORSSerialPortManager.shared().availablePorts
        
        DispatchQueue.global().async {
            for serialPort in serialPorts {
                let sema = DispatchSemaphore(value: 0)
                
                self.genDevice(with: serialPort) { device in
                    if let device = device {
                        if !self.devices.contains(device) {
                            self.devices += [device]
                            self.post(name: .UMDeviceDidConnected, userInfo: [UMDeviceCenter.connectedDeviceKey: device])
                        }
                    } else if let device = self.device(with: serialPort) {
                        if let index = self.devices.firstIndex(of: device) {
                            self.devices.remove(at: index)
                            self.post(name: .UMDeviceDidDisconnected, userInfo: [UMDeviceCenter.disconnectedDeviceKey: device])
                        }
					}
					
                    sema.signal()
                }
                
                sema.wait()
            }
            
            for device in self.devices where !serialPorts.contains(device.connection.serialPort) {
                if let index = self.devices.firstIndex(of: device) {
                    self.devices.remove(at: index)
                    self.post(name: .UMDeviceDidDisconnected, userInfo: [UMDeviceCenter.disconnectedDeviceKey: device])
                }
            }
            
            if !self.isCancelling {
                Thread.sleep(forTimeInterval: 1)
                self.refreshDevices()
            } else {
                self.isCancelling = false
                self.isScanning = false
                self.devices = []
                self.post(name: .UMDeviceCenterDidStopScanning)
            }
        }
    }
    
    public func startScanning() {
        guard !isScanning else { return }
        isScanning = true
        refreshDevices()
    }
    
    public func stopScanning() {
        guard isScanning && !isCancelling else { return }
        isCancelling = true
    }
    
}
