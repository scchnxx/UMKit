extension Packet.KeyCombo {

    enum KeyEvent: Int {
        case none = 0x00
        case down = 0x40
        case up   = 0x80
        case long = 0xC0
    }

    enum KeyCode: Int {
        case defaultKey
        case upBtn
        case down
        case left
        case right
        case enter
        case esc
        case delegate
        case dZoomIn
        case dZoomOut
        case snapshot
        case power
        case menu
        
        case remoteCTRL = 0x0F
        case oZoomIn
        case oZoomOut
        case step_oZoomIn
        case step_oZoomOut
        case afManual
        case afSingle
        case afContinuous
        case afStart
        case afStop
        case ledOnOff
        case ledInc
        case ledDec
        case backForthTest
        case routeInit
        case posiRestore
        case returnZero
        case lensType
        case afArea
        case ledType
        case ledMode
        case lencIndex
        case motorOffset
        case evInc
        case evDec
        case disp
        case videoRatio
        case sharpness
        case freeze
        case awb
        case lencRST
        
        case assignPos = 0x2E
    }
    
}

extension Packet {

    struct KeyCombo: Equatable {
        
        static func keyDown(with keyCode: KeyCode) -> KeyCombo {
            return KeyCombo(keyCode: keyCode, keyEvent: .down)
        }
        
        static func keyUp(with keyCode: KeyCode) -> KeyCombo {
            return KeyCombo(keyCode: keyCode, keyEvent: .up)
        }
        
        static func long(with keyCode: KeyCode) -> KeyCombo {
            return KeyCombo(keyCode: keyCode, keyEvent: .long)
        }
        
        var keyCode: KeyCode
        var keyEvent: KeyEvent
        var rawValue: Int { keyCode.rawValue | keyEvent.rawValue }
        
        init(keyCode: KeyCode, keyEvent: KeyEvent) {
            self.keyCode = keyCode
            self.keyEvent = keyEvent
        }
    }
    
}
