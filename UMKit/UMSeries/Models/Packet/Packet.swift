struct Packet {
    
    enum PacketType {
        case readValue
        case readChar
        case writeValue
        
        fileprivate var prefixChar: Character {
            switch self {
            case .readValue:  return "R"
            case .readChar:   return "S"
            case .writeValue: return "W"
            }
        }
    }
    
    var type: PacketType
    
    var address: Address
    
    var value: Int
    
    var data: Data
    
    /// Data prefix: R
    init(readValue address: Address) {
        self.type = .readValue
        self.address = address
        self.value = 0x00
        let addr = String(format: "%04X", address.rawValue)
        data = "R\(addr)\r".data(using: .utf8)!
    }
    
    /// Data prefix: S
    init(readChar address: Address) {
        self.type = .readChar
        self.address = address
        self.value = 0x00
        let addr = String(format: "%04X", address.rawValue)
        data = "S\(addr)\r".data(using: .utf8)!
    }
    
    /// Data prefix: W
    init(writeValue address: Address, value: Int) {
        self.type = .writeValue
        self.address = address
        self.value = value
        let addr = String(format: "%04X", (address.rawValue << 8 | value))
        data = "W\(addr)\r".data(using: .utf8)!
    }
    
    /// Data prefix: W
    init(writeValue address: Address, keyCombo: KeyCombo) {
        self.type = .writeValue
        self.address = address
        self.value = keyCombo.rawValue
        let addr = String(format: "%04X", (address.rawValue << 8 | value))
        data = "W\(addr)\r".data(using: .utf8)!
    }
    
}
