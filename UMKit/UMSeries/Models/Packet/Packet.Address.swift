extension Packet {
    
    enum Address: Int {
        // Other flags
        case staflag = 0x00
        case keyco
        case keych
        case afsta
        case mtsta
        case ledmax
        case ledlv
        case magnih
        case magnil
        case oposih
        case oposil
        case routeh
        case routel
        case lenstyp
        case afarea // (14)10
        // Ev
        case ev = 0x16
        case assignPosH = 0x18
        case assignPosL = 0x19
        // Serial number
        case sn9 = 0x23
        case sn8
        case sn7
        case sn6
        case sn5
        case sn4
        case sn3
        case sn2
        case sn1
        case sn0
        // Firmware version
        case ver2 = 0x2D
        case ver1
        case ver0
        
        case test = 0xFFF
    }
    
}
