extension UMDevice.Action {
    
    var attributeUpdateSettings: [UMDevice.AttributeUpdateTimerSettings] {
        switch self {
        case .nextRemoteControlState:
            return [(.status, 0.5, true)]
            
        case .resetMotor, .assignPosition, .zoomIn, .zoomOut, .stepZoomIn, .stepZoomOut:
            return [(.position, 0.5, true)]
            
        case .ledSwitch, .increaseLedLevel, .decreaseLedLevel:
            return [(.ledLevel, 0.5, true)]
            
        case .autoFocusManually, .autoFocus, .autoFocusContinuously, .stopAotuFocus:
            return [(.position, 0.5, true)]
            
        case .nextEV, .lastEV:
            return [(.ev, 1, true)]
            
        default:
            return []
        }
    }
    
}
