extension UMDevice.DeviceModel {
    
    func evDescription_UM08_UM20(_ ev: Int) -> String? {
        switch ev {
        case 0:  return "-9"
        case 1:  return "-8"
        case 2:  return "-7"
        case 3:  return "-6"
        case 4:  return "-5"
        case 5:  return "-4"
        case 6:  return "-3"
        case 7:  return "FLK"
        case 8:  return "-1"
        case 9:  return "Auto"
        case 10: return "+1"
        case 11: return "+2"
        case 12: return "+3"
        case 13: return "+4"
        case 14: return "+5"
        case 15: return "+6"
        case 16: return "+7"
        case 17: return "+8"
        case 18: return "+9"
        default: return nil
        }
    }
    
    func evDescription_UM08A(_ ev: Int) -> String? {
        switch ev {
        case 0:  return "-10"
        case 1:  return "-9"
        case 2:  return "-8"
        case 3:  return "-7"
        case 4:  return "-6"
        case 5:  return "-5"
        case 6:  return "-4"
        case 7:  return "FLK"
        case 8:  return "-2"
        case 9:  return "-1"
        case 10: return "Auto"
        case 11: return "+1"
        case 12: return "+2"
        case 13: return "+3"
        case 14: return "+4"
        case 15: return "+5"
        case 16: return "+6"
        case 17: return "+7"
        case 18: return "+8"
        case 19: return "+9"
        default: return nil
        }
    }
    
    func evDescription_UM10(_ ev: Int) -> String? {
        switch ev {
        case 0:  return "-9"
        case 1:  return "-8"
        case 2:  return "-7"
        case 3:  return "-6"
        case 4:  return "-5"
        case 5:  return "-4"
        case 6:  return "-3"
        case 7:  return "FLK"
        case 8:  return "-1"
        case 9:  return "Auto"
        case 10: return "+1"
        case 11: return "+2"
        case 12: return "+3"
        case 13: return "+4"
        case 14: return "+5"
        case 15: return "+6"
        case 16: return "+7"
        case 17: return "+8"
        case 18: return "+9"
        default: return nil
        }
    }
    
}
