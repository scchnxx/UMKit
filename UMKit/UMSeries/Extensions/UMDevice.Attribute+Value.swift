extension UMDevice.Attribute {
    
    func checkValueType(value: Any) -> Bool {
        switch self {
        case .serialNumber:  return (value is String)
        case .version:       return (value is String)
        case .status:        return (value is UMDevice.Status)
        case .position:      return (value is Int)
        case .ledLevel:      return (value is Int)
        case .lensType:      return (value is UMDevice.LensType)
        case .autoFocusArea: return (value is UMDevice.AutoFocusArea)
        case .ev:            return (value is Int)
        }
    }
    
    func getValue(with data: [String]) -> Any? {
        switch self {
        case .status:        return status(data: data)
        case .position:      return position(data: data)
        case .ledLevel:      return ledLevel(data: data)
        case .lensType:      return lensType(data: data)
        case .autoFocusArea: return autoFocusArea(data: data)
        case .ev:            return ev(data: data)
        default:             return nil
        }
    }
    
    func serialNumber(data: [String]) -> String? {
        guard data.count == packets().count else { return nil }
        return data.reduce("", { $0 + $1 })
    }
    
    func version(data: [String]) -> String? {
        guard data.count == packets().count else { return nil }
        return data.joined(separator: ".")
    }
    
    func status(data: [String]) -> UMDevice.Status? {
        guard data.count == 1, let dataInt = UInt(data[0], radix: 16) else { return nil }
        let suffix = String(dataInt, radix: 2)
        let prefix = String(repeating: "0", count: 5 - suffix.count)
        let bin = prefix + suffix
        let bools = bin.map({ $0 == "1" })
        return UMDevice.Status(values: bools)
    }
    
    func ledLevel(data: [String]) -> Int? {
        guard data.count == 1 else { return nil }
        return Int(data[0], radix: 16)
    }
    
    func position(data: [String]) -> Int? {
        guard data.count == 2 else { return nil }
        guard let hi = Int(data[0], radix: 16), let lo = Int(data[1], radix: 16) else { return nil }
        return hi << 8 | lo
    }
    
    func lensType(data: [String]) -> UMDevice.LensType? {
        guard data.count == 1 else { return nil }
        guard let rawValue = Int(data[0]) else { return nil }
        return UMDevice.LensType(rawValue: rawValue)
    }
    
    func autoFocusArea(data: [String]) -> UMDevice.AutoFocusArea? {
        guard data.count == 1 else { return nil }
        guard let rawValue = Int(data[0], radix: 16) else { return nil }
        return UMDevice.AutoFocusArea(rawValue: rawValue)
    }
    
    func ev(data: [String]) -> Int? {
        guard data.count == 1 else { return nil }
        return Int(data[0], radix: 16)
    }
    
}
