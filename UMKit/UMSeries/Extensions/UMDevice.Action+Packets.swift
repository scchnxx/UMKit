extension UMDevice.Action {
    
    func packets() -> [Packet] {
        switch self {
        case .nextRemoteControlState:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .remoteCTRL))
            ]
            
        case .resetMotor:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .posiRestore))
            ]
                
        case .assignPosition(let position):
            return [
                Packet(writeValue: .assignPosH, value: position >> 8),
                Packet(writeValue: .assignPosL, value: position & 0xFF),
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .assignPos))
            ]
                
        case .zoomIn(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .oZoomIn, keyEvent: pressed ? .down : .up))
            ]
            
        case .zoomOut(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .oZoomOut, keyEvent: pressed ? .down : .up))
            ]
            
        case .stepZoomIn(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .step_oZoomIn, keyEvent: pressed ? .down : .up))
            ]
            
        case .stepZoomOut(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .step_oZoomOut, keyEvent: pressed ? .down : .up))
            ]
            
        case .ledSwitch:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .ledOnOff))
            ]
            
        case .increaseLedLevel(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .ledInc, keyEvent: pressed ? .down : .up))
            ]
            
        case .decreaseLedLevel(let pressed):
            return [
                Packet(writeValue: .keyco, keyCombo: .init(keyCode: .ledDec, keyEvent: pressed ? .down : .up))
            ]
            
        case .setAutoFocusArea(let area):
            return [
                Packet(writeValue: .afarea, value: area.rawValue)
            ]
                
        case .autoFocusManually:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .afManual)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .afManual))
            ]
            
        case .autoFocus:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .afSingle)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .afSingle))
            ]
            
        case .autoFocusContinuously:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .afContinuous)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .afContinuous))
            ]
               
        case .stopAotuFocus:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .afStop)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .afStop))
            ]
            
        case .freeze:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .freeze))
            ]
            
        case .nextEV:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .evInc))
            ]
            
        case .lastEV:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .evDec))
            ]
            
        case .sharpness:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .awb))
            ]
            
        case .awb:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .sharpness))
            ]
            
        case .nextDisplayedInfo:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .disp)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .disp))
            ]
            
        case .nextStatus:
            return [
                Packet(writeValue: .keyco, keyCombo: .long(with: .disp))
            ]
            
        case .videoRatio:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .videoRatio))
            ]

        case .menu:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .menu))
            ]
            
        case .up:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .upBtn)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .upBtn))
            ]
            
        case .left:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .left)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .left))
            ]
            
        case .right:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .right)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .right))
            ]
            
        case .down:
            return [
                Packet(writeValue: .keyco, keyCombo: .keyDown(with: .down)),
                Packet(writeValue: .keyco, keyCombo: .keyUp(with: .down))
            ]
        }
    }
    
}
