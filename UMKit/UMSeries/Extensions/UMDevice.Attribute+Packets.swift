extension UMDevice.Attribute {
    
    func packets() -> [Packet] {
        switch self {
        case .serialNumber:
            return (0...9).map { Packet.Address(rawValue: Packet.Address.sn0.rawValue - $0)! }.map { Packet(readChar: $0) }
            
        case .version:
            return (0...2).map { Packet.Address(rawValue: Packet.Address.ver0.rawValue - $0)! }.map { Packet(readValue: $0) }
            
        case .status:
            return [Packet(readValue: .staflag)]
            
        case .ledLevel:
            return [Packet(readValue: .ledlv)]
            
        case .position:
            return [Packet(readValue: .oposih), Packet(readValue: .oposil)]
            
        case .lensType:
            return [Packet(readValue: .lenstyp)]
            
        case .autoFocusArea:
            return [Packet(readValue: .afarea)]
            
        case .ev:
            return [Packet(readValue: .ev)]
        }
    }
    
}
