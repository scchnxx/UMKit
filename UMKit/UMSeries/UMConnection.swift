import ORSSerial

class UMConnection: Connection {
    
    private func extractData(from responseData: Data) -> Data? {
        guard responseData.count >= 3 else { return nil }
        let start = responseData.index(after: responseData.startIndex)
        let end = responseData.index(before: responseData.endIndex)
        return responseData[start..<end]
    }
    
    func fetch(with packets: [Packet],
               _ completion: @escaping (Result<[String], ConnectionError>) -> Void)
    {
        let descriptor = ORSSerialPacketDescriptor(prefixString: "@",
                                                     suffixString: "\r",
                                                     maximumPacketLength: 4,
                                                     userInfo: nil)
        
        let data = packets.map { packet in
            packet.data
        }
        
        sendRequest(data: data, descriptor: descriptor) { result in
            switch result {
            case .success(let dataCollection):
                let resultData = dataCollection.compactMap { data in
                    self.extractData(from: data)
                }.compactMap { data in
                    String(data: data, encoding: .utf8)
                }
                completion(.success(resultData))
                
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    
}
