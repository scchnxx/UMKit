import ORSSerial

public protocol UMDeviceDelegate: AnyObject {
    func deviceDidActivate(_ device: UMDevice)
    func deviceDidFailToActivate(_ device: UMDevice)
    func deviceDidDeactivate(_ device: UMDevice)
    func deviceDidFailToDeactivate(_ device: UMDevice)
    func device(_ device: UMDevice, didUpdate attribute: UMDevice.Attribute)
}

extension UMDevice.DeviceModel {
    
    init?(serialNumber: String) {
        switch serialNumber.uppercased() {
        case "UM08TW0100":
            self = .UM08
            
        case "UM08TW01A0":
            self = .UM08_A
            
        case "UM20TW01A0":
            self = .UM20
            
        case "UM10TW0100", "UM10TW01A0":
            self = .UM10
            
        default:
            return nil
        }
    }
    
}

extension UMDevice.Status {
    
    /// Init Status using the given boolean values.
    init?(values: [Bool]) {
        guard values.count == 5 else { return nil }
        isFinished   = values[4]
        ledOff       = values[3]
        isFocusing   = values[2]
        isActive  = values[1]
        isForwarding = values[0]
    }
    
}

extension UMDevice {

    public enum DeviceModel: Int, CaseIterable, CustomStringConvertible {
        case UM08
        case UM08_A
        case UM20
        case UM10
        
        public var description: String {
            switch self {
            case .UM08:   return "UM08"
            case .UM08_A: return "UM08A"
            case .UM20:   return "UM20"
            case .UM10:   return "UM10"
            }
        }
        
        public func evDescription(ev: Int) -> String? {
            switch self {
            case .UM08, .UM20: return evDescription_UM08_UM20(ev)
            case .UM08_A: return evDescription_UM08A(ev)
            case .UM10: return evDescription_UM10(ev)
            }
        }
        
    }

    public struct Status: Equatable {
        public var isFinished: Bool
        public var ledOff: Bool
        public var isFocusing: Bool
        public var isActive: Bool
        public var isForwarding: Bool
    }
    
    public enum LensType: Int, CaseIterable, CustomStringConvertible {
        case x4
        case x10
        case fl
        
        public var description: String {
            switch self {
            case .x4:  return "x4"
            case .x10: return "x10"
            case .fl:  return "FL"
            }
        }
    }
    
    public enum AutoFocusArea: Int {
        case area01
        case area02
        case area03
        case area04
        case area05
        case area06
        case area07
        case area08
        case area09
        case area10
        case area11
        case area12
        case area13
        case area14
        case area15
        case area16
        case area17
        case area18
        case area19
        case area20
        case area21
        case area22
        case area23
        case area24
        case area25
        case area26
        case areaAll
        case areaLocal
    }
    
    public enum Attribute: CaseIterable {
        case serialNumber
        case version
        case status
        case ledLevel
        case position
        case lensType
        case autoFocusArea
        case ev
    }
    
    public enum Action: Equatable {
        case nextRemoteControlState
        case resetMotor
        case assignPosition(Int)
        case zoomIn(Bool)
        case zoomOut(Bool)
        case stepZoomIn(Bool)
        case stepZoomOut(Bool)
        case ledSwitch
        case increaseLedLevel(Bool)
        case decreaseLedLevel(Bool)
        case setAutoFocusArea(AutoFocusArea)
        case autoFocusManually
        case autoFocus
        case autoFocusContinuously
        case stopAotuFocus
        case freeze
        case nextEV
        case lastEV
        case sharpness
        case awb
        case nextDisplayedInfo
        case nextStatus
        case videoRatio
        
        case menu
        case up
        case left
        case right
        case down
    }
    
    typealias AttributeUpdateTimerSettings = (attribute: Attribute, interval: TimeInterval, autoDismiss: Bool)
    
}

public class UMDevice: Equatable {
    
    public weak var delegate: UMDeviceDelegate?
    
    public static func == (lhs: UMDevice, rhs: UMDevice) -> Bool {
        lhs.connection == rhs.connection
    }
    
    private var updateInterval: TimeInterval = 0.3
    private var updateThreshold = 3
    private var updateTimers = [Attribute: (timer: Timer, count: Int)]()
    // Activation / Deactivation
    private var timeoutInterval: TimeInterval = 2
    
    private var isActivating = false
    private var activationHandler: ((Bool) -> Void)?
    private var activationTimeoutTimer: Timer?
    
    private var isDeactivating = false
    private var deactivationHandler: ((Bool) -> Void)?
    private var deactivationTimeoutTimer: Timer?
	
	private var handleTimeoutTimer: Timer?
    
    var connection: UMConnection
    
    public var deviceModel: DeviceModel
    public var serialNumber: String
    public var version: String
    public var isActive = false
    public private(set) var attributes = [Attribute: Any]()
    
    init(connection: UMConnection, serialNumber: String, version: String, deviceModel: DeviceModel) {
        self.connection = connection
        self.deviceModel = deviceModel
        self.serialNumber = serialNumber
        self.version = version
        
        switch deviceModel {
        case .UM08_A, .UM10, .UM20:
            let handShakingDesc = ORSSerialPacketDescriptor(packetData: "#\r".data(using: .utf8)!,
                                                            userInfo: nil)
            connection.startListen(to: handShakingDesc) { (descr, data) in
				self.handleTimeoutTimer?.invalidate()
				
                let data = "#".data(using: .utf8)!
				let timer = Timer(timeInterval: 1, repeats: true) { _ in
					self.isActive = false
					self.delegate?.deviceDidDeactivate(self)
				}
				
                connection.send(data: data, timeoutInterval: 0)
				self.handleTimeoutTimer = timer
				RunLoop.main.add(timer, forMode: .common)
            }
        
        default:
            break
        }
    }
    
    // MARK : Fetching
    
    private func extractData(from responseData: Data) -> Data? {
        guard responseData.count >= 3 else { return nil }
        let start = responseData.index(after: responseData.startIndex)
        let end = responseData.index(before: responseData.endIndex)
        return responseData[start..<end]
    }
    
    private func shouldUpdateAttribute(newValue: Any, forKey attribute: Attribute) -> Bool {
        guard attribute.checkValueType(value: newValue) else { return false }
        switch attribute {
        case .serialNumber:  return (attributes[attribute] as? String) != (newValue as? String)
        case .version:       return (attributes[attribute] as? String) != (newValue as? String)
        case .status:        return (attributes[attribute] as? Status) != (newValue as? Status)
        case .position:      return (attributes[attribute] as? Int) != (newValue as? Int)
        case .ledLevel:      return (attributes[attribute] as? Int) != (newValue as? Int)
        case .lensType:      return (attributes[attribute] as? LensType) != (newValue as? LensType)
        case .autoFocusArea: return (attributes[attribute] as? AutoFocusArea) != (newValue as? AutoFocusArea)
        case .ev:            return (attributes[attribute] as? Int) != (newValue as? Int)
        }
    }
    
    private func updateAttribute(value: Any, forKey attribute: Attribute) {
        guard shouldUpdateAttribute(newValue: value, forKey: attribute) else {
            if let count = updateTimers[attribute]?.count {
                if count == updateThreshold {
                    stopAttributeUpdating(attribute: attribute)
                } else {
                    updateTimers[attribute]?.count = (count + 1)
                }
            }
            return
        }
        
        if updateTimers[attribute] != nil {
            updateTimers[attribute]?.count = 0
        }
        
        attributes[attribute] = value
        
        if attribute != .status, !hasAttributeUpdater(for: .status) {
            startAttributeUpdater(settings: (.status, updateInterval, true))
        }
		
        delegate?.device(self, didUpdate: attribute)
    }
	
	private func updateStatus(_ status: Status) {
		let activationStateChanged = (self.isActive != status.isActive)
		
		self.isActive = status.isActive
		
		if self.isActive {
			if self.isActivating {
				self.didActivate()
			}
			if activationStateChanged {
				self.delegate?.deviceDidActivate(self)
			}
		} else {
			if self.isDeactivating {
				self.didDeactivate()
			}
			if activationStateChanged {
				self.delegate?.deviceDidDeactivate(self)
			}
		}
	}
	
	public func fetch(_ attribute: Attribute, _ completion: ((Any?) -> Void)? = nil) {
		guard connection.isOpen else {
			completion?(nil)
			return
		}
		
		connection.fetch(with: attribute.packets()) { result in
			switch result {
			case .success(let data):
				if let value = attribute.getValue(with: data) {
					self.updateAttribute(value: value, forKey: attribute)
					if attribute == .status, let status = value as? Status {
						self.updateStatus(status)
					}
					completion?(value)
				} else {
					self.stopAttributeUpdating(attribute: attribute)
					completion?(nil)
				}
				
			case .failure:
				completion?(nil)
			}
		}
	}
    
    // MARK: - Performing
    
    private func hasAttributeUpdater(for attribute: Attribute) -> Bool {
        updateTimers[attribute] != nil
    }
    
    private func startAttributeUpdater(settings: AttributeUpdateTimerSettings) {
        stopAttributeUpdating(attribute: settings.attribute)
        let timer = Timer(timeInterval: settings.interval, repeats: true) { _ in
            self.fetch(settings.attribute)
        }
        updateTimers[settings.attribute] = (timer, 0)
        RunLoop.main.add(timer, forMode: .common)
    }
    
    private func stopAttributeUpdating(attribute: Attribute) {
		guard let info = updateTimers.removeValue(forKey: attribute) else { return }
        info.timer.invalidate()
    }
    
    @discardableResult
    public func internalPerform(action: Action) -> Bool {
        guard connection.isOpen else { return false }
        
        let data = action.packets().map { packet in
            packet.data
        }
        
        connection.send(data: data)
        
        for settings in action.attributeUpdateSettings {
            startAttributeUpdater(settings: settings)
        }
        
        return true
    }
    
    @discardableResult
    public func perform(action: Action) -> Bool {
        guard isActive else { return false }
        return internalPerform(action: action)
    }
    
    // MARK: - Remote Control
    
    private func didActivate() {
        isActivating = false
        activationTimeoutTimer?.invalidate()
        activationHandler?(true)
    }
    
    private func didFailToActivate() {
        isActivating = false
        activationTimeoutTimer?.invalidate()
		delegate?.deviceDidFailToActivate(self)
        activationHandler?(false)
    }
    
    public func activate(_ completion: ((Bool) -> Void)? = nil) {
        guard connection.isOpen, !isActivating, !isActive else {
            completion?(false)
            return
        }
        
        isActivating = true
        activationHandler = completion
        
        let group = DispatchGroup()
        
        for attr in Attribute.allCases.reversed() {
            group.enter()
            fetch(attr) { _ in
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            guard (self.attributes[.status] as? Status)?.isActive != true else {
				self.didActivate()
				return
			}
            
            let timer = Timer(timeInterval: self.timeoutInterval, repeats: false) { _ in
                if self.isActivating {
                    self.didFailToActivate()
                }
            }
            
            self.activationTimeoutTimer = timer
            self.internalPerform(action: .nextRemoteControlState)
            
            RunLoop.main.add(timer, forMode: .common)
        }
    }
    
    private func didDeactivate() {
        isDeactivating = false
        deactivationTimeoutTimer?.invalidate()
        deactivationHandler?(true)
    }
    
    private func didFailToDeactivate() {
        isDeactivating = false
        deactivationTimeoutTimer?.invalidate()
        delegate?.deviceDidFailToDeactivate(self)
        deactivationHandler?(false)
    }
    
    public func deactivate(_ completion: ((Bool) -> Void)? = nil) {
        guard connection.isOpen, !isDeactivating, isActive else {
			completion?(false)
			return
		}
        
		handleTimeoutTimer?.invalidate()
		handleTimeoutTimer = nil
		
        let timer = Timer(timeInterval: timeoutInterval, repeats: false) { _ in
            if self.isDeactivating {
                self.didFailToDeactivate()
            }
        }
		
        isDeactivating = true
        deactivationHandler = completion
        deactivationTimeoutTimer = timer
		handleTimeoutTimer?.invalidate()
        internalPerform(action: .nextRemoteControlState)
        
        RunLoop.main.add(timer, forMode: .common)
    }
    
}
