import ORSSerial

protocol ConnectionDelegate: AnyObject {
    func connectionDidOpen(_ con: Connection)
    func connectionDidFailToOpen(_ con: Connection)
    func connectionDidClose(_ con: Connection)
    func connectionDidRemove(_ con: Connection)
}

enum ConnectionError: Error {
    case serialPortClosed
    case requestTimedOut
}

class Connection: NSObject {
    
    private struct Response {
        var espectedDataLength: Int
        var dataCollection: [Data]
        var handle: ((Result<[Data], ConnectionError>) -> Void)?
    }
    
    weak var delegate: ConnectionDelegate?
    
    private var sendQueue = DispatchQueue(label: "com.scchnxx.UMKit.Connection.sendQueue")
    private var sendInterval: TimeInterval = 0.05
    
    // Serial Port
    var serialPort: ORSSerialPort!
    // Handlers
    private var openHandler: ((Bool) -> Void)?
    // Data
    private var receivedResponsesForIDs = [UUID: Response]()
    private var descriptorHandlers = [ORSSerialPacketDescriptor: (ORSSerialPacketDescriptor, Data) -> Void]()
    
    // Properties
    let uuid = UUID()
    var name: String {
        serialPort.name
    }
    var isOpen: Bool {
        serialPort.isOpen
    }
    
    init(serialPort port: ORSSerialPort) {
        super.init()
        
        serialPort = port
        serialPort.baudRate = 115200
        serialPort.numberOfStopBits = 2
        serialPort.parity = .none
        serialPort.delegate = self
    }
    
    func startListen(to descriptor: ORSSerialPacketDescriptor, _ handler: @escaping (ORSSerialPacketDescriptor, Data) -> Void) {
        descriptorHandlers[descriptor] = handler
        serialPort.startListeningForPackets(matching: descriptor)
    }
    
    func stopListen(descriptor: ORSSerialPacketDescriptor) {
        descriptorHandlers[descriptor] = nil
        serialPort.stopListeningForPackets(matching: descriptor)
    }
    
    func open(_ completion: ((Bool) -> Void)?) {
        guard !serialPort.isOpen else {
            completion?(true)
            return
        }
        openHandler = completion
        serialPort.open()
    }
    
    @discardableResult
    func close() -> Bool {
        guard serialPort.isOpen else { return true }
        return serialPort.close()
    }
    
    @discardableResult
    func send(data: Data, timeoutInterval: TimeInterval, userInfo: Any? = nil, descriptor: ORSSerialPacketDescriptor? = nil) -> Bool {
        guard serialPort.isOpen else { return false }
        let request = ORSSerialRequest(dataToSend: data,
                                       userInfo: userInfo,
                                       timeoutInterval: timeoutInterval,
                                       responseDescriptor: descriptor)
        
        sendQueue.async {
            self.serialPort.send(request)
            Thread.sleep(forTimeInterval: self.sendInterval)
        }
        return true
    }
    
    func sendRequest(data: [Data], descriptor: ORSSerialPacketDescriptor,
                     _ completion: @escaping  (Result<[Data], ConnectionError>) -> Void)
    {
        guard serialPort.isOpen else {
            completion(.failure(.serialPortClosed))
            return
        }
        
        let id = UUID()
        receivedResponsesForIDs[id] = Response(espectedDataLength: data.count, dataCollection: [], handle: completion)
        
        for data in data {
            self.send(data: data, timeoutInterval: 0.05, userInfo: id, descriptor: descriptor)
        }
    }
    
    @discardableResult
    func send(data: [Data]) -> Bool {
        guard serialPort.isOpen else { return false }
        
        for data in data {
            self.send(data: data, timeoutInterval: 0)
        }
        
        return true
    }
    
}

extension Connection: ORSSerialPortDelegate {
    
    func serialPortWasRemovedFromSystem(_ serialPort: ORSSerialPort) {
        delegate?.connectionDidRemove(self)
    }
    
    func serialPortWasClosed(_ serialPort: ORSSerialPort) {
        receivedResponsesForIDs.removeAll()
        delegate?.connectionDidClose(self)
    }
    
    func serialPortWasOpened(_ serialPort: ORSSerialPort) {
        openHandler?(true)
        openHandler = nil
        delegate?.connectionDidOpen(self)
    }
    
    func serialPort(_ serialPort: ORSSerialPort, didEncounterError error: Error) {
        if openHandler != nil {
            openHandler?(false)
            openHandler = nil
            delegate?.connectionDidFailToOpen(self)
        }
    }
    
    func serialPort(_ serialPort: ORSSerialPort, didReceivePacket packetData: Data, matching descriptor: ORSSerialPacketDescriptor) {
        if let handler = descriptorHandlers[descriptor] {
            handler(descriptor, packetData)
        }
    }
    
    func serialPort(_ serialPort: ORSSerialPort, didReceiveResponse responseData: Data, to request: ORSSerialRequest) {
        guard let id = request.userInfo as? UUID,
            var response = receivedResponsesForIDs[id] else { return }
        
        response.dataCollection.append(responseData)
        
        if response.dataCollection.count == response.espectedDataLength {
            receivedResponsesForIDs[id] = nil
            response.handle?(.success(response.dataCollection))
        } else {
            receivedResponsesForIDs[id] = response
        }
    }
    
    func serialPort(_ serialPort: ORSSerialPort, requestDidTimeout request: ORSSerialRequest) {
        guard let id = request.userInfo as? UUID, let response = receivedResponsesForIDs[id] else { return }
        
        receivedResponsesForIDs[id] = nil
        response.handle?(.failure(.requestTimedOut))
    }
    
}
